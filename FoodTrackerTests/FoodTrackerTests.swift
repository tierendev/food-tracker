//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Sothearith Sreang on 3/7/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {
    
    //MARK: Meal class tests
    
    // Confirm that meal initialization class returns the correct object when passed
    // with correct inputs
    func testMealInitializationSuceeds() {
        let zeroRatingMeal = Meal.init(name: "Meal 1", image: nil, rating: 0)
        XCTAssertNotNil(zeroRatingMeal)
        
        let highestRatingMeal = Meal.init(name: "Meal 2", image: nil, rating: 5)
        XCTAssertNotNil(highestRatingMeal)
    }
    
    // Confirm that meal initialization class returns nil object when passed
    // with incorrect inputs
    func testMealInitializationFails() {
        // Empty title
        let emptyTitleMeal = Meal.init(name: "", image: nil, rating: 0)
        XCTAssertNil(emptyTitleMeal)
        
        // Negative rating
        let negativeRatingMeal = Meal.init(name: "Meal 1", image: nil, rating: -4)
        XCTAssertNil(negativeRatingMeal)
        
        // Rating > 5
        let tooBigRatingMeal = Meal.init(name: "Meal 2", image: nil, rating: 6)
        XCTAssertNil(tooBigRatingMeal)
    }
}
