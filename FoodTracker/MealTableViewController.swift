//
//  MealTableViewController.swift
//  FoodTracker
//
//  Created by Sothearith Sreang on 8/7/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit
import os.log

class MealTableViewController: UITableViewController {
    
    // MARK: Constants
    private let CELL_IDENTIFIER = "MealTableViewCell"
    
    // MARK: Properties
    var meals = [Meal]()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        
        loadSampleMeals()
    }
    
    // MARK: Actions
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? AddMealViewController, let meal = sourceViewController.meal {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update meal if existed
                meals[selectedIndexPath.row] = meal
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                // Add a new meal
                let newIndexPath = IndexPath(row: meals.count, section: 0)
                meals.append(meal)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            case "AddItem":
                os_log("Adding a new meal.", log: OSLog.default, type: .debug)
            
            case "ShowDetail":
                guard let mealDetailViewDestination = segue.destination as? AddMealViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
            }
                guard let selectedMealCell = sender as? MealTableViewCell else {
                    fatalError("Unexpected sender: \(sender)")
            }
            
                guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                    fatalError("Unexpected indexPath: \(selectedMealCell)")
            }
            
                let selectedMeal = meals[indexPath.row]
                mealDetailViewDestination.meal = selectedMeal
            
            default:
                fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }
    

    // MARK: Private functions
    private func loadSampleMeals() {
        let meal1Photo = UIImage(named: "meal1")
        let meal2Photo = UIImage(named: "meal2")
        let meal3Photo = UIImage(named: "meal3")
        
        meals += [
            createSampleMeal(name: "Caprese Salad", image: meal1Photo, rating: 4),
            createSampleMeal(name: "Chicken and Potatoes", image: meal2Photo, rating: 5),
            createSampleMeal(name: "Pasta with Meatballs", image: meal3Photo, rating: 3)
        ]
    }
    
    private func createSampleMeal(name: String, image: UIImage?, rating: Int) -> Meal {
        guard let meal = Meal(name: name, image: image, rating: rating) else {
            fatalError("Unable to instantiate \(name)")
        }
        return meal
    }
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as? MealTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let meal = meals[indexPath.row]
        cell.mealLabel.text = meal.name
        cell.mealPhoto.image = meal.image
        cell.mealRating.rating = meal.rating
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            meals.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            break
            
        case .insert:
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            break
            
        default:
            os_log("Unrecognized editStyle")
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
