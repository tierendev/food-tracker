//
//  Meal.swift
//  FoodTracker
//
//  Created by Sothearith Sreang on 8/7/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

class Meal {
    
    var name: String
    var image: UIImage?
    var rating: Int
    
    init?(name: String, image: UIImage?, rating: Int) {
        
        // Name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // Rating must be between 0 and 5 inclusively
        guard rating >= 0 && rating <= 5 else {
            return nil
        }
        
        self.name = name
        self.image = image
        self.rating = rating
    }
}
